import style from '../styles/Home.module.css';

export default function Home() {
  return (
      <>
        <div className={style.mainContainer}>
          <h1> { process.env.NEXT_PUBLIC_ENV_NAME }</h1>
        </div>
      </>
  )
}


const nextConfig = {
  reactStrictMode: true,
  exportPathMap: async function(
      defaultPathMap,
      { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      "/": { page: "/" },
    };
  },
}

module.exports = nextConfig
